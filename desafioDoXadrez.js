const xadrez = [
    [4,3,2,5,6,2,3,4],
    [1,1,1,1,1,1,1,1],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [1,1,1,1,1,1,1,1],
    [4,3,2,5,6,2,3,4]
]

function allChessPieces(arrayOfArrays) {
        let pieces = [];
        for (let i = 0; i < arrayOfArrays.length; i++) {
            for (let j = 0; j < arrayOfArrays[i].length; j++) {
                pieces.push(arrayOfArrays[i][j]);
            }          
        }
        return pieces;
}

function isItPawn(chessPiece) {
    return chessPiece === 1;
}

function isItBishop(chessPiece) {
    return chessPiece === 2;
}

function isItKnight(chessPiece) {
    return chessPiece === 3;
}

function isItRook(chessPiece) {
    return chessPiece === 4;
}

function isItQueen(chessPiece) {
    return chessPiece === 5;
}

function isItKing(chessPiece) {
    return chessPiece === 6;
}

const allPieces = allChessPieces(xadrez);

const peao = allPieces.filter(isItPawn).length;
const bispo = allPieces.filter(isItBishop).length;
const cavalo = allPieces.filter(isItKnight).length;
const torre = allPieces.filter(isItRook).length;
const rainha = allPieces.filter(isItQueen).length;
const rei = allPieces.filter(isItKing).length;


console.log(`Peão: ${peao} peça(s)`)
console.log(`Bispo: ${bispo} peça(s)`)
console.log(`Cavalo: ${cavalo} peça(s)`)
console.log(`Torre: ${torre} peça(s)`)
console.log(`Rainha: ${rainha} peça(s)`)
console.log(`Rei: ${rei} peça(s)`)
